package com.michal5111.bicycles.services;

import com.michal5111.bicycles.entities.Bicycle;
import com.michal5111.bicycles.entities.BicycleStation;
import com.michal5111.bicycles.entities.Hire;
import com.michal5111.bicycles.entities.Stand;
import com.michal5111.bicycles.repositories.BicycleRepository;
import com.michal5111.bicycles.repositories.BicycleStationRepository;
import com.michal5111.bicycles.repositories.HireRepository;
import com.michal5111.bicycles.repositories.StandRepository;
import com.michal5111.bicycles.requests.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

@org.springframework.stereotype.Service
public class Service {

    private final BicycleStationRepository bicycleStationRepository;

    private final StandRepository standRepository;

    private final BicycleRepository bicycleRepository;

    private final HireRepository hireRepository;

    @Autowired
    public Service(BicycleStationRepository bicycleStationRepository, StandRepository standRepository, BicycleRepository bicycleRepository, HireRepository hireRepository) {
        this.bicycleStationRepository = bicycleStationRepository;
        this.standRepository = standRepository;
        this.bicycleRepository = bicycleRepository;
        this.hireRepository = hireRepository;
    }

    public void createStation(CreateStationRequest createStationRequest) {
        if (bicycleStationRepository.existsByName(createStationRequest.getName())) {
            throw new IllegalArgumentException("Bicycle station with that name already exists!");
        }
        if (createStationRequest.getName().equals("")) {
            throw new IllegalArgumentException("Name cannot be empty!");
        }
        BicycleStation bicycleStation = new BicycleStation(createStationRequest.getName());
        bicycleStation = bicycleStationRepository.save(bicycleStation);
        if (createStationRequest.getStandCount() != null) {
            for (int i = 0; i < createStationRequest.getStandCount(); i++) {
                standRepository.save(new Stand(bicycleStation));
            }
        }
    }

    public void deleteStation(DeleteStationRequest deleteStationRequest) {
        Optional<BicycleStation> bicycleStation = bicycleStationRepository.findByName(deleteStationRequest.getName());
        if (!bicycleStation.isPresent()) {
            throw new IllegalArgumentException("There is no station by that name!");
        }
        List<Bicycle> bicycleList = bicycleRepository.findAllByBicycleStationId(bicycleStation.get().getId());
        bicycleList.forEach(bicycle -> bicycle.setBicycleStation(null));
        standRepository.deleteAllByBicycleStationId(bicycleStation.get().getId());
        bicycleStationRepository.deleteBicycleStationByName(deleteStationRequest.getName());
    }

    public void modifyStation(ModifyStationRequest modifyStationRequest) {
        Optional<BicycleStation> optionalBicycleStation = bicycleStationRepository.findByName(modifyStationRequest.getName());
        if (!optionalBicycleStation.isPresent()) {
            throw new IllegalArgumentException("There is no station by that name!");
        }
        BicycleStation bicycleStation = optionalBicycleStation.get();
        if (modifyStationRequest.getNewName() != null) {
            if (modifyStationRequest.getNewName().equals("")) {
                throw new IllegalArgumentException("Name cannot be empty!");
            }
            bicycleStation.setName(modifyStationRequest.getNewName());
            bicycleStationRepository.save(bicycleStation);
        }
        if (modifyStationRequest.getSize() != null) {
            if (!(modifyStationRequest.getSize() < 0)) {
                Stack<Stand> standStack = standRepository.findAllByBicycleStation_NameOrderByBicycle_IdDesc(bicycleStation.getName());
                int newSize = modifyStationRequest.getSize();
                if (standStack.size() > newSize) {
                    for (int i = standStack.size() - 1; i >= newSize; i--) {
                        standRepository.delete(standStack.pop());
                    }
                } else if (standStack.size() < newSize) {
                    for (int i = 0; i < newSize - standStack.size(); i++) {
                        standRepository.save(new Stand(bicycleStation));
                    }
                }
            } else throw new IllegalArgumentException("Size cannot be less then zero!");
        }
    }

    public List<Map<String, String>> stationsStatus() {
        return bicycleStationRepository.getStatus();
    }

    public Hire hireBicycle(HireBicycleRequest hireBicycleRequest) {
        Optional<Bicycle> optionalBicycle =
                bicycleRepository.findFirstByBicycleStation_Name(hireBicycleRequest.getStationName());
        if (!optionalBicycle.isPresent()) {
            throw new IllegalArgumentException("There is no bicycle at this station!");
        }
        Bicycle bicycle = optionalBicycle.get();
        Hire hire = Hire.builder()
                .bicycle(bicycle)
                .startBicycleStation(bicycle.getBicycleStation()).build();
        Optional<Stand> optionalStand = standRepository.findByBicycle_Id(bicycle.getId());
        if (optionalStand.isPresent()) {
            optionalStand.get().setBicycle(null);
            standRepository.save(optionalStand.get());
        }
        hireRepository.save(hire);
        bicycle.setBicycleStation(null);
        bicycleRepository.save(bicycle);
        return hire;
    }

    public void returnBicycle(ReturnBicycleRequest returnBicycleRequest) {
        Optional<Bicycle> optionalBicycle = bicycleRepository.findById(returnBicycleRequest.getBicycleId());
        Optional<BicycleStation> optionalBicycleStation = bicycleStationRepository.findByName(returnBicycleRequest.getStationName());
        if (!optionalBicycle.isPresent() || !optionalBicycleStation.isPresent()) {
            throw new IllegalArgumentException("There is no bicycle with this id or bicycle station with that name!");
        }
        Bicycle bicycle = optionalBicycle.get();
        BicycleStation bicycleStation = optionalBicycleStation.get();
        Optional<Hire> optionalHire = hireRepository.findByBicycle_IdAndStopBicycleStationIsNull(bicycle.getId());
        if (!optionalHire.isPresent()) {
            throw new IllegalArgumentException("Unknown hire!");
        }
        Hire hire = optionalHire.get();
        hire.setStopBicycleStation(bicycleStation);
        bicycle.setBicycleStation(bicycleStation);
        Optional<Stand> optionalStand = standRepository.findFirstByBicycleIsNullAndBicycleStation_Name(bicycleStation.getName());
        if (optionalStand.isPresent()) {
            Stand stand = optionalStand.get();
            stand.setBicycle(bicycle);
            standRepository.save(stand);
        }
        bicycleRepository.save(bicycle);
        hireRepository.save(hire);
    }
}
