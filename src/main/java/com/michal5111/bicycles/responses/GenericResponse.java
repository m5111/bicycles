package com.michal5111.bicycles.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GenericResponse {
    private Boolean success;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    public GenericResponse(Boolean success) {
        this.success = success;
    }
}
