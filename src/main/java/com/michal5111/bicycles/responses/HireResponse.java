package com.michal5111.bicycles.responses;

import com.michal5111.bicycles.entities.Hire;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HireResponse {
    String success;
    Hire hire;
}
