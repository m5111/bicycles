package com.michal5111.bicycles.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ModifyStationRequest {
    private String name;
    private String newName;
    private Integer size;
}
