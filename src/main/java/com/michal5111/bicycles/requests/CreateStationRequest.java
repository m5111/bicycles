package com.michal5111.bicycles.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateStationRequest implements Serializable {
    private String name;
    private Integer standCount;
}
