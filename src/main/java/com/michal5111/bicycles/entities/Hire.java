package com.michal5111.bicycles.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Hire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bicycle_id")
    private Bicycle bicycle;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "start_bicycle_station_id", referencedColumnName = "id")
    private BicycleStation startBicycleStation;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "stop_bicycle_station_id", referencedColumnName = "id")
    private BicycleStation stopBicycleStation;
}
