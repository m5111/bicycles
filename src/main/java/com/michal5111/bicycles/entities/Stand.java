package com.michal5111.bicycles.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
public class Stand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @OrderColumn(name = "bicycle_station_id")
    private BicycleStation bicycleStation;

    @OneToOne
    @JoinColumn(name = "bicycle_id")
    private Bicycle bicycle;


    public Stand(BicycleStation bicycleStation) {
        this.bicycleStation = bicycleStation;
    }
}
