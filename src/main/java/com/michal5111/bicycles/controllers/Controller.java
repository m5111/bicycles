package com.michal5111.bicycles.controllers;

import com.michal5111.bicycles.entities.Hire;
import com.michal5111.bicycles.requests.*;
import com.michal5111.bicycles.responses.GenericResponse;
import com.michal5111.bicycles.responses.HireResponse;
import com.michal5111.bicycles.services.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@org.springframework.stereotype.Controller
public class Controller {

    private Service service;

    @Autowired
    public Controller(Service service) {
        this.service = service;
    }

    @Transactional
    @PostMapping("/stations/create")
    public ResponseEntity createStation(@RequestBody CreateStationRequest createStationRequest) {
        try {
            service.createStation(createStationRequest);
            return ResponseEntity.status(201).body(new GenericResponse(true));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(new GenericResponse(false, ex.getMessage()));
        }
    }

    @Transactional
    @DeleteMapping("/stations/delete")
    public ResponseEntity deleteStation(@RequestBody DeleteStationRequest deleteStationRequest) {
        try {
            service.deleteStation(deleteStationRequest);
            return ResponseEntity.ok("{\"success\":true}");
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(new GenericResponse(false, ex.getMessage()));
        }
    }

    @PutMapping("/stations/modify")
    public ResponseEntity modifyStation(@RequestBody ModifyStationRequest modifyStationRequest) {
        try {
            service.modifyStation(modifyStationRequest);
            return ResponseEntity.ok(new GenericResponse(true));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(new GenericResponse(false, ex.getMessage()));
        }
    }

    @GetMapping("/stations/status")
    public ResponseEntity stationsStatus() {
        return new ResponseEntity<>(service.stationsStatus(), HttpStatus.OK);
    }

    @PostMapping("bicycle/hire")
    public ResponseEntity hireBicycle(@RequestBody HireBicycleRequest hireBicycleRequest) {
        try {
            Hire hire = service.hireBicycle(hireBicycleRequest);
            HireResponse hireResponse = new HireResponse("true", hire);
            return ResponseEntity.ok(hireResponse);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(new GenericResponse(false, ex.getMessage()));
        }
    }

    @PutMapping("bicycle/return")
    public ResponseEntity returnBicycle(@RequestBody ReturnBicycleRequest returnBicycleRequest) {
        try {
            service.returnBicycle(returnBicycleRequest);
            return ResponseEntity.ok(new GenericResponse(true));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(new GenericResponse(false, ex.getMessage()));
        }
    }
}
