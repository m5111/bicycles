package com.michal5111.bicycles.repositories;

import com.michal5111.bicycles.entities.Bicycle;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface BicycleRepository extends Repository<Bicycle, Long> {
    Bicycle save(Bicycle bicycle);

    Optional<Bicycle> findById(Long id);

    Optional<Bicycle> findFirstByBicycleStation_Name(String name);

    List<Bicycle> findAllByBicycleStationId(Long id);
}
