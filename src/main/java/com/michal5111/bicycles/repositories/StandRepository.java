package com.michal5111.bicycles.repositories;

import com.michal5111.bicycles.entities.Stand;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.Stack;

public interface StandRepository extends Repository<Stand, Long> {

    Stand save(Stand stand);

    void deleteAllByBicycleStationId(Long id);

    Optional<Stand> findFirstByBicycleIsNullAndBicycleStation_Name(String name);

    Optional<Stand> findByBicycle_Id(Long id);

    Stack<Stand> findAllByBicycleStation_NameOrderByBicycle_IdDesc(String name);

    void delete(Stand stand);
}
