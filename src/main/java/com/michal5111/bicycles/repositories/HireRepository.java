package com.michal5111.bicycles.repositories;

import com.michal5111.bicycles.entities.Hire;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface HireRepository extends Repository<Hire, Long> {
    Hire save(Hire hire);

    Optional<Hire> findByBicycle_IdAndStopBicycleStationIsNull(Long id);
}
