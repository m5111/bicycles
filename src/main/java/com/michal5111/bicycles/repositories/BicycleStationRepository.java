package com.michal5111.bicycles.repositories;

import com.michal5111.bicycles.entities.BicycleStation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface BicycleStationRepository extends Repository<BicycleStation, Long> {

    BicycleStation save(BicycleStation bicycleStation);

    Optional<BicycleStation> findByName(String name);

    void deleteBicycleStationByName(String name);

    boolean existsByName(String name);

    @Query(value = "SELECT name, " +
            "(SELECT count(id) FROM stand WHERE bicycle_station_id = b.id and bicycle_id is null) as free_stands, " +
            "(SELECT count(id) FROM stand WHERE bicycle_station_id = b.id and bicycle_id is not null) as taken_stands, " +
            "(SELECT count(id) FROM bicycle WHERE bicycle_station_id = b.id) as bicycles " +
            "FROM bicycle_station b", nativeQuery = true)
    List<Map<String, String>> getStatus();
}
